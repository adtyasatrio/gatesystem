#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>  // Install this library via Arduino Library Manager

const char* ssid = "lucky7";
const char* password = "182182182";
// const char* server = "192.168.86.29";
const char* server = "192.168.86.21";
const int port = 6000;

const char* apiCheckCardEndpoint = "/api/checkcard/";
const char* apiAddCardEndpoint = "/api/addcard/";
const char* apiAddLogEndpoint = "/api/addlog/";

// Pin connected to the data output of the card sensor
const int tempSensorPin = A0;

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }

  Serial.println("Connected to WiFi");
}

void loop() {
  // Read card from sensor
  float card_uid = readCard();

  // Check if the card is already in the database
  if (checkCardInDB(card_uid)) {
    Serial.println("Gate is OPEN!!");

    // Create JSON payload
    String payload = createJsonLogPayload(card_uid);

    // Send data to server
    WiFiClient client;
    sendLogData(client, payload);
  } else {
    Serial.println("Card not found in the database.");

    // Create JSON payload
    String payload = createJsonLogPayload(card_uid);

    // Send data to server
    WiFiClient client;
    sendLogData(client, payload);
  }

  // Wait for 1 minute before checking/sending the next data
  // delay(30000);
}

int readCard() {
  // Replace this with your actual code to read card from the sensor
  return random(0, 10);  // Placeholder, replace with actual sensor reading
}

void printCardInfo(JsonObject cardData) {
  int id = cardData["id"];
  String name = cardData["name"];
  String address = cardData["address"];
  String cardUid = cardData["card_uid"];

  Serial.println("ID: " + String(id));
  Serial.println("Name: " + name);
  Serial.println("Address: " + address);
  Serial.println("Card UID: " + cardUid);
}

bool checkCardInDB(int card_uid) {
  // Construct the complete URL for checking the card in the database
  String url = "http://" + String(server) + ":" + String(port) + apiCheckCardEndpoint + card_uid;

  Serial.println("Checking card in the database: " + url);

  // Use the HTTPClient to send a GET request
  WiFiClient client;
  HTTPClient http;
  http.begin(client, url);

  int httpResponseCode = http.GET();

  if (httpResponseCode == 200) {
    // Parse JSON response
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, http.getString());

    if (error) {
      Serial.println("Error parsing JSON: " + String(error.c_str()));
      return false;
    }

    // Extract information from JSON
    bool success = doc["success"];
    String message = doc["message"];

    if (success) {
      JsonObject data = doc["data"];

      // Print information
      Serial.println("Card found in the database (HTTP 200).");
      printCardInfo(data);

      return true;
    } else {
      Serial.println("Card not found in the database. Message: " + message);
      return false;
    }
  } else if (httpResponseCode == 404) {
    // Serial.println("Card not found in the database (HTTP 404).");
    return false;
  } else {
    Serial.println("Error checking card in the database (HTTP " + String(httpResponseCode) + ").");
    return false;
  }
}

String createJsonLogPayload(float card_uid) {
  // Create a JSON document
  DynamicJsonDocument doc(1024);

  // Add data to the document
  doc["card_uid"] = card_uid;

  // Serialize the JSON document to a string
  String jsonLogPayload;
  serializeJson(doc, jsonLogPayload);

  return jsonLogPayload;
}

String createJsonCardPayload(float card_uid) {
  // Create a JSON document
  DynamicJsonDocument doc(1024);

  // Add data to the document
  doc["card_uid"] = card_uid;

  // Serialize the JSON document to a string
  String jsonCardPayload;
  serializeJson(doc, jsonCardPayload);

  return jsonCardPayload;
}

void sendLogData(WiFiClient& client, String payload) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    // Construct the complete URL for adding log to the database
    String url = "http://" + String(server) + ":" + String(port) + apiAddLogEndpoint;

    Serial.println("Sending data to: " + url);
    Serial.println("Payload: " + payload);

    http.begin(client, url);
    http.addHeader("Content-Type", "application/json");

    // POST the data
    int httpResponseCode = http.POST(payload);

    if (httpResponseCode > 0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    } else {
      Serial.print("HTTP Error: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  } else {
    Serial.println("WiFi not connected. Unable to send data.");
  }
}

void sendCardData(WiFiClient& client, String payload) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    // Construct the complete URL for adding card to the database
    String url = "http://" + String(server) + ":" + String(port) + apiAddCardEndpoint;

    Serial.println("Sending data to: " + url);
    Serial.println("Payload: " + payload);

    http.begin(client, url);
    http.addHeader("Content-Type", "application/json");

    // POST the data
    int httpResponseCode = http.POST(payload);

    if (httpResponseCode > 0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    } else {
      Serial.print("HTTP Error: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  } else {
    Serial.println("WiFi not connected. Unable to send data.");
  }
}