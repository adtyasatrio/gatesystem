const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const db = require("./config/database");

const app = express();
const PORT = process.env.PORT || 6000;

// set body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// create log data / insert log data
app.post("/api/addlog", (req, res) => {
  const data = { ...req.body };
  const querySql = "INSERT INTO logs SET ?";

  db.query(querySql, data, (err, rows, field) => {
    if (err) {
      return res
        .status(500)
        .json({ message: "Gagal insert data!", error: err });
    }

    res.status(200).json({ success: true, message: "Berhasil insert data!" });
  });
});

// create card data / insert card data
app.post("/api/addcard", (req, res) => {
  const data = { ...req.body };
  const querySql = "INSERT INTO cards SET ?";

  db.query(querySql, data, (err, rows, field) => {
    if (err) {
      return res
        .status(500)
        .json({ message: "Gagal insert data!", error: err });
    }

    res.status(200).json({ success: true, message: "Berhasil insert data!" });
  });
});

// check card data
app.get("/api/checkcard/:uid", async (req, res) => {
  const uid = req.params.uid;
  const querySql = "SELECT * FROM cards WHERE card_uid = ?";

  db.query(querySql, [uid], (err, rows, fields) => {
    if (err) {
      return res
        .status(500)
        .json({ message: "Error querying card data!", error: err });
    }

    if (rows.length === 0) {
      return res
        .status(404)
        .json({ success: false, message: "Card data not found!" });
    }

    res
      .status(200)
      .json({ success: true, message: "Card data found!", data: rows[0] });
  });
});

app.get("/api/triggerCapture", async (req, res) => {
  try {
    const esp32Response = await axios.get("http://ESPCAM_IP_ADDRESS/capture");
    console.log("ESP32 Response:", esp32Response.data);
    res.json({
      success: true,
      message: "Image capture triggered successfully",
    });
  } catch (error) {
    res
      .status(500)
      .json({
        success: false,
        message: "Internal Server Error",
        error: error.message,
      });
  }
});

// create server
app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));
